{
  description = "social.coop Dev Setup";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {nixpkgs, flake-utils, ...}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        ansible = pkgs.ansible;
        ansible-collections = pkgs.callPackage ./ansible-collections.nix {} ansible {
          community-general = {
            version = "6.1.0";
            sha256 = "sha256:14wb5adw7bi8l763s8bj1s5ar0c7vx4vyqhy1rq16k16lxvvd0c3";
          };
          community-docker = {
            version = "3.3.2";
            sha256 = "sha256:14psjcdkcpq58zsc7vn7lbc0mw356g8v17kpbzxi7kpn8m8rc9y1";
          };
        };
      in rec {
        packages = rec {
          dev-env = pkgs.buildEnv {
            name = "social.coop-dev-env";
            paths = [
              ansible
              ansible-collections
              pkgs.pass
            ];
          };
        };
        defaultPackage = packages.dev-env;
    });
}

