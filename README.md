# social.coop/tech/env-nix

Dev environment using [nix](https://nixos.org/)

## Prerequisites

* nix package manager (NixOS is not necessary for this)
* [nix flakes](https://nixos.wiki/wiki/Flakes) must be enabled
* [direnv](https://direnv.net/)

## How to use

1. Create a workspace directory for all the social.coop things
1. Clone this repository in the workspace
1. Clone the [pass repository](https://git.coop/social.coop/tech/pass) in the workspace
1. Create .envrc at the root of the workspace with following contents:
   ```bash
   nix build ./env-nix --out-link $PWD/.env

   export PATH=$PWD/.env/bin:$PATH
   export ANSIBLE_COLLECTIONS_PATH=$PWD/.env/ansible_collections
   export PASSWORD_STORE_DIR=$PWD/pass
   ```
1. direnv allow

## Caveats

Nix has a steep learning curve. So, this might _"waste"_ more time than save.
